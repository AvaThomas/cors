package cors

import (
	"gitee.com/billcoding/flygo"
	"testing"
)

func TestCors(t *testing.T) {
	app := flygo.NewApp()
	app.UseFilter(New().Methods("GET"))
	app.Get("/", func(c *flygo.Context) {
		c.Text("index")
	})
	app.Run()
}

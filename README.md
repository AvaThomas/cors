# cors

A Cors middleware for flygo

## Usage
```
app := flygo.NewApp()
app.UseFilter(cors.New())
app.Get("/", func(c flygo.Context) {
    c.Text("index")
})
app.Run()
```

## Config
* Origin ：`Access-Control-Allow-Origin`, default `*`
```
cors.Origin("http://domain.com")
```

* Methods ：`Access-Control-Allow-Methods`, default `GET,POST,PUT,DELETE,PATCH`
```
cors.Methods("GET", "POST")
```

* Headers ：`Access-Control-Allow-Headers`
```
cors.Headers("X-Requested-With")
```

* Extra Headers
```
cors.ExtraHeaders(cors.Header("Access-Control-Max-Age", "86400"))
```